import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
/**
* @author Claire_Justin_Yassine
* @version v2 du 04 mai 2021
*/
public class Page2 extends JFrame
{
    private JButton bouton11;
    private JButton bouton12;
    private JButton bouton13;
    private JButton bouton14;
    private Page21 page21;
    private Page22 page22;
    private Page23 page23;
    
    
    public Page2()
    {
        JFrame f2 = new JFrame();
        this.setSize(600, 300);
        this.setLocationRelativeTo(null);
        this.setTitle("Action Pilotes");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        JPanel p = new JPanel();
        p.setBackground(Color.BLACK);
        this.setContentPane(p);
        this.setVisible(true);
        
        JButton bouton21 = new JButton("Liste des Pilotes");
        bouton21.setPreferredSize(new Dimension(250, 130));
        JButton bouton22 = new JButton("Choisir un pilote");
        bouton22.setPreferredSize(new Dimension(250, 130));
        JButton bouton23 = new JButton("Ajouter un pilote");
        bouton23.setPreferredSize(new Dimension(250, 130));
        JButton bouton24 = new JButton("Quitter");
        bouton24.setPreferredSize(new Dimension(250, 130));
        p.add(bouton21);
        p.add(bouton22);
        p.add(bouton23);
        p.add(bouton24);
        
        // Bouton Lister tous les pilotes
        bouton21.addActionListener(
        new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if ((JButton)e.getSource() == bouton21)
                {
                    new Page21().setVisible(true);
                }
            }
        }
        );
        
        // Bouton Identifier un pilote
        bouton22.addActionListener(
        new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if ((JButton)e.getSource() == bouton22)
                {
                    new Page22().setVisible(true);
                }
            }
        }
        );
       
        // Bouton Créer un nouveau pilote
        bouton23.addActionListener(
        new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if ((JButton)e.getSource() == bouton23)
                {
                    new Page23().setVisible(true);
                }
            }
        }
        );
        
        // Bouton Quitter
        bouton24.addActionListener(
        new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if ((JButton)e.getSource() == bouton24)
                {
                    System.exit(0);
                }
            }
        }
        );
    }

} 