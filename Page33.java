 import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import javax.swing.JComboBox;
import javax.swing.BoxLayout;

/**
* @author Claire_Justin_Yassine
* @version v2 du 04 mai 2021
*/
public class Page33 extends JFrame
{
    private JButton bouton331 = new JButton("Valider");
    private JButton bouton332 = new JButton("Annuler");
    private JPanel p = new JPanel();
    private JComboBox<String> type = new JComboBox<String>();
    private JComboBox<String> etat = new JComboBox<String>();
    
    public Page33()
    {
        this.setSize(600, 300);
        this.setLocationRelativeTo(null);
        this.setTitle("Créer un nouveau chasseur");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setVisible(true);
        
        p.add(bouton331, BorderLayout.SOUTH);
        p.add(bouton332, BorderLayout.NORTH);
        
        type.addItem("x-wing");
        type.addItem("y-wing");
        p.add(type);
        
        etat.addItem("Opérationnel");
        etat.addItem("En maintenance");
        etat.addItem("Détruit");
        p.add(etat);
        
        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
        
        setContentPane(p);
        
        // Créer une nouvelle commande
        Cmd cmd = new Cmd();
        
        // Bouton Valider
        bouton331.addActionListener(
        new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    if ((JButton)e.getSource() == bouton331)
                    {
                        //TODO
                        String typee = (String)type.getSelectedItem();
                        String etatt = (String)etat.getSelectedItem();
                        
                        cmd.creerUnChasseur(typee, etatt); // Crée & ajoute un chasseur au hangar
                        
                        // Commandes à mettre dans les 2 dernières fenetres
                        cmd.afficherDesChasseurs(); // Affiche tous les chasseurs du hangar
                        cmd.nbChasseursOperationnels(); // Afficher le nb de chasseurs opérationels
                    }
                }
            }
        );
        
        // Bouton Quitter
        bouton332.addActionListener(
        new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if ((JButton)e.getSource() == bouton332)
                {
                    System.exit(0);
                }
            }
        }
        );
    }
}
