import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import javax.swing.JComboBox;
import javax.swing.BoxLayout;

/**
* @author Claire_Justin_Yassine
* @version v2 du 04 mai 2021
*/
public class Page23 extends JFrame
{
    private JButton bouton231 = new JButton("Valider");
    private JButton bouton232 = new JButton("Annuler");
    private JPanel p = new JPanel();
    private JTextField nom = new JTextField("Nom du pilote");
    private JTextField prenom = new JTextField("Prénom du pilote");
    private JTextField age = new JTextField("Age du pilote");
    private JComboBox<String> race = new JComboBox<String>();
    
    public Page23()
    {
        setSize(600, 300);
        setLocationRelativeTo(null);
        setTitle("Nouveau pilote");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        this.setVisible(true);
        
        p.setLayout(new BorderLayout());
        
        p.add(bouton231, BorderLayout.SOUTH);
        p.add(bouton232, BorderLayout.NORTH);
        
        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
        p.add(nom);
        p.add(prenom);
        p.add(age);
        
        race.addItem("Chalactéens");
        race.addItem("Chiss");
        race.addItem("Humains");
        race.addItem("Ithoriens");
        race.addItem("Kel'Dor");
        race.addItem("Kiffars");
        race.addItem("Miralukas");
        race.addItem("Mirialans");
        race.addItem("Nagais");
        race.addItem("Neimoidiens");
        race.addItem("Niktos");
        race.addItem("Noghris");
        race.addItem("Ongrees");
        race.addItem("Pau'ans");
        race.addItem("Quermiens");
        race.addItem("Rakata");
        race.addItem("Rodiens");
        race.addItem("Thisspasiens");
        race.addItem("Togrutas");
        race.addItem("Wookies");
        race.addItem("Wronians");
        race.addItem("Zabraks");
        
        p.add(race);
        
        setContentPane(p);
        
        // Créer une nouvelle commande
        Cmd cmd = new Cmd();
        
        // Bouton Valider
        bouton231.addActionListener(
        new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    if ((JButton)e.getSource() == bouton231)
                    {
                        //TODO
                        String nomm = nom.getText().trim();
                        String prenomm = prenom.getText().trim();
                        int agee = Integer.parseInt(age.getText().trim());
                        String racee = (String)race.getSelectedItem();
                        
                        cmd.creerUnPilote(nomm, prenomm, agee, racee); // Crée & ajoute un chasseur au hangar
                    }
                }
            }
        );
        
        // Bouton Annuler
        bouton232.addActionListener(
        new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    if ((JButton)e.getSource() == bouton232)
                    {
                        System.exit(0);
                    }
                }
            }
        );
    }
} 