import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
/**
* @author Claire_Justin_Yassine
* @version v2 du 04 mai 2021
*/
public class Page1 extends JFrame
{
    private JButton bouton1;
    private JButton bouton2;
    private Page2 page2;
    private Page3 page3;

    public Page1()
    {
        JFrame f1 = new JFrame();
        this.setSize(600, 300);
        this.setLocationRelativeTo(null);
        this.setTitle("Flotte des rebelles");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        
        JPanel p = new JPanel();
        p.setBackground(Color.BLACK);
        this.setContentPane(p);
        this.setVisible(true);
        
        JButton bouton1 = new JButton("Pilotes");
        bouton1.setPreferredSize(new Dimension(250, 280));
        JButton bouton2 = new JButton("Chasseur");
        bouton2.setPreferredSize(new Dimension(250, 280));
        p.add(bouton1);
        p.add(bouton2);
        
        bouton1.addActionListener(
        new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if ((JButton)e.getSource() == bouton1)
                {
                    new Page2().setVisible(true);
                }
            }
        }
        );
        
        bouton2.addActionListener(
        new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if ((JButton)e.getSource() == bouton2)
                {
                    new Page3().setVisible(true);
                }
            }
        }
        );
    }
}