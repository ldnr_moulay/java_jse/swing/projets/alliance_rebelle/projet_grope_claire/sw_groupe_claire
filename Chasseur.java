import java.util.ArrayList;

/**
 * Décrivez votre classe Chasseur ici.
 *
 * @author Claire
 * @version v2 du 04 mai 2021
 */
public class Chasseur
{
    // variables d'instance
    // private int identifiant;
    private String type;
    private String etat;
    private Pilote pilote;
    private boolean b;
    private static ArrayList<Chasseur> hangar = new ArrayList<Chasseur>();

    /**
     * Constructeur d'objets de classe Chasseur
     */
    public Chasseur(String type, String etat)
    {
        // initialisation des variables d'instance
        // this.identifiant = identifiant;
        //this.type = type;
        this.etat = etat;
        this.pilote = null;
        this.b = true;
        hangar.add(this);  
        
        // Mettre le type en minuscule
        type = type.toLowerCase();
        
        // Vérifier que le type saisi fait parti des types acceptés
        if (type.equals("x-wing") || type.equals("y-wing")) {
            this.type = type;
        }
        
        /* // Mettre l'état en minuscule
        etat = etat.toLowerCase();
        
        // Enlever les accents ???
        
        // Enlever les espaces
        etat.replaceAll("\\s","");
        
        // Vérifier que l'état saisi fait parti des types acceptés
        if (etat.equals("opérationnel") || etat.equals("enmaintenance") 
            || etat.equals("détruit")) {
            this.etat = etat;
        } */
    }
    
    /**
     * Getter : hangar
     * Retourner la liste des chasseurs créés
     */
    
    public static ArrayList<Chasseur> getHangar() {
        return hangar;
    }

    /**
     * Getter : type
     * Retourner le type de chasseur
     */
    public String getType()
    {
        return type;
    }
    
    /**
     * Getter : état
     * Retourner l'état du chasseur
     */
    public String getEtat()
    {
        return etat;
    }
    
    /**
     * Getter : pilote
     * Retourner le pilote du chasseur
     */
    public Pilote getPilote()
    {
        return pilote;
    }
    
    /**
     * Setter : type
     * Modifier le type de chasseur
     */
    public void setEtat(String input)
    {
        this.etat = input;
    }
    
    /**
     * Setter : pilote
     * Modifier le pilote du chasseur
     */    
    public void setPilote() {
        int id1 = 0;
        int id2 = 0;
        if (estDisponible() == true) {
            this.pilote = Pilote.getEscadrille().get(id1);
            if (pilote.getChasseur() != Chasseur.getHangar().get(id2)) {
                pilote.setChasseur();
            }
        }
    }
    
    /**
     * Vérifier que le chasseur est disponible pour etre attribué à un pilote
     * Si le chasseur est disponible, alors il est pourra etre associé à un pilote
     */
    
    public boolean estDisponible() {
        if (this.pilote != null) {
            b = false;
        }
        return b;
    }
    
    /**
     * Afficher un chasseur (méthode non utilisée)
     */
    
    @Override
    public String toString() {
        return "Type : " + type + ", Etat " + etat;
    }
    
    /**
     * Afficher un chasseur - v2
     */
    
    public void afficher() {
        System.out.print(", Type : " + getType() + ", Etat " + getEtat());
        
        if (this.pilote != null) {
            System.out.println(", piloté par " + getPilote().getNom());
        } else {
            System.out.println(", piloté par " + getPilote());
        }
    }
}
