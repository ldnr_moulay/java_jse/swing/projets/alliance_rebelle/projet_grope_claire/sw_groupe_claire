import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import javax.swing.JComboBox;
import javax.swing.BoxLayout;
import javax.swing.JLabel; 
/**
* Décrivez votre classe Page21 ici.
*
* @author Justin
* @version v3 du 05 mai 2021
*/
public class Page21 extends JFrame
{
    private JPanel p = new JPanel();
    
    public Page21()
    {
        setSize(600, 300);
        setLocationRelativeTo(null);
        setTitle("Liste des pilotes");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setVisible(true);
    }
}