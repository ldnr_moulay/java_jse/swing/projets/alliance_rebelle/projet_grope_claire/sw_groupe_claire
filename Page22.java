import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import javax.swing.JComboBox;
import javax.swing.BoxLayout;
import javax.swing.JLabel;

/**
* Décrivez votre classe Page22 ici.
*
* @author Claire_Justin_Yassine
* @version v2 du 04 mai 2021
*/
public class Page22 extends JFrame
{
    private JButton bouton221 = new JButton("Valider");
    private JButton bouton222 = new JButton("Quitter");
    private JPanel p = new JPanel();
    private JTextField id = new JTextField("Saisir id");
    private JLabel label = new JLabel("Quelle est l'identification de votre pilote ?");
    
    public Page22() {
        setSize(600, 300);
        setLocationRelativeTo(null);
        setTitle("Identifier un pilote");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setVisible(true);
        
        p.add(label);
        p.add(id);
        p.add(bouton221);
        p.add(bouton222);
        
        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
        setContentPane(p);
        
        // Créer une nouvelle commande
        Cmd cmd = new Cmd();
        
        // Bouton Valider
        bouton221.addActionListener(
        new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if ((JButton)e.getSource() == bouton221)
                {
                //Afficher les caractéristiques du pilote à l'identifiant choisi (index -1).
                int identifiant = Integer.parseInt(id.getText().trim());
                cmd.afficherUnPilote(identifiant);
                }
            }
        }
        );
        
        // Bouton Quitter
        bouton222.addActionListener(
        new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if ((JButton)e.getSource() == bouton222)
                {
                    System.exit(0);
                }
            }
        }
        );
    }
} 