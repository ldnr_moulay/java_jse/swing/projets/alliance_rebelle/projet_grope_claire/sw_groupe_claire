import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import javax.swing.JComboBox;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JTextArea;

/**
* Décrivez votre classe Page32 ici.
*
* @author Claire_Justin_Yassine
* @version v2 du 04 mai 2021
*/
public class Page32 extends JFrame
{
    private JButton bouton321 = new JButton("Valider");
    private JButton bouton322 = new JButton("Quitter");
    private JPanel p = new JPanel();
    private JTextField id = new JTextField("Saisir id");
    private JLabel label = new JLabel("Quelle est l'identification de votre chasseur ?");
    
    private JTextArea jta1;
    private JTextArea jta2;
    private JTextArea jta3;
    int index;
    
    public Page32()
    {
        setSize(600, 300);
        setLocationRelativeTo(null);
        setTitle("Identifier un chasseur");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setVisible(true);
        
        p.add(label);
        p.add(id);
        p.add(bouton321);
        p.add(bouton322);
        
        // Ajouter une zone de texte pour l'identifiant du chasseur
        jta1 = new JTextArea("Identifiant = " + index);
        jta1.setEditable(false);
        p.add(jta1);
        
        // Ajouter une zone de texte pour le type du chasseur
        jta2 = new JTextArea("Type = ");
        jta2.setEditable(false);
        p.add(jta2);
        
        // Ajouter une zone de texte pour l'état du chasseur
        jta3 = new JTextArea("Etat = ");
        jta3.setEditable(false);
        p.add(jta3);
        
        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
        setContentPane(p);
        
        // Créer une nouvelle commande
        Cmd cmd = new Cmd();
        
        // Bouton Valider
        bouton321.addActionListener(
        new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if ((JButton)e.getSource() == bouton321)
                {
                    //Afficher les caractéristiques du chasseur à l'identifiant choisi (index -1).
                    int identifiant = Integer.parseInt(id.getText().trim());
                    cmd.afficherUnChasseur(identifiant);
                    jta1.setText("Identifiant = " + identifiant);
                }
            }
        }
        );
        
        // Bouton Quitter
        bouton322.addActionListener(
        new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if ((JButton)e.getSource() == bouton322)
                {
                    System.exit(0);
                }
            }
        }
        );
    }
} 