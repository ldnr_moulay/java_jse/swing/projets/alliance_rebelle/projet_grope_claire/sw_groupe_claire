import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import javax.swing.JComboBox;
import javax.swing.BoxLayout;
import javax.swing.JLabel;

/**
* Décrivez votre classe Page34 ici.
* @author Claire_Justin_Yassine
* @version v2 du 04 mai 2021
*/
public class Page34 extends JFrame
{
    private JButton bouton341 = new JButton("Valider");
    private JButton bouton342 = new JButton("Quitter");
    private JPanel p = new JPanel();
    private JTextField id = new JTextField("Saisir id");
    private JLabel label1 = new JLabel("Quelle est l'identification de votre chasseur ?");
    private JLabel label2 = new JLabel("Quel est l'état de votre chasseur ?");
    private JComboBox<String> etat = new JComboBox<String>();
    
    /**
    * Constructeur d'objets de classe Page34
    */
    public Page34()
    {
        setSize(600, 300);
        setLocationRelativeTo(null);
        setTitle("Modifier l'état d'un chasseur");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setVisible(true);
        
        p.add(label1);
        p.add(id);
        p.add(label2);
        
        etat.addItem("Opérationnel");
        etat.addItem("En maintenance");
        etat.addItem("Détruit");
        p.add(etat);
        
        p.add(bouton341);
        p.add(bouton342);
        
        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
        setContentPane(p);
        
        // Créer une nouvelle commande
        Cmd cmd = new Cmd();
        
        bouton341.addActionListener(
        new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if ((JButton)e.getSource() == bouton341)
                {
                    //Modifie l'état du chasseur ayant l'identifiant choisi
                    int identifiant = Integer.parseInt(id.getText().trim());
                    String etatt = (String)etat.getSelectedItem();
                    
                    cmd.modifierEtatChasseur(identifiant, etatt);
                }
            }
        }
        );
        
        bouton342.addActionListener(
        new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if ((JButton)e.getSource() == bouton342)
                {
                    System.exit(0);
                }
            }
        }
        );
    }
}