/**
 * Décrivez votre classe AjouterNvChasseur ici.
 *
 * @author Claire & Yassine
 * @version v2 du 04 mai 2021
 */
public class Cmd
{   
    // PARTIE 1 : COMMANDES EN LIEN AVEC LA CLASSE CHASSEUR
    /**
     * Créer & ajouter un nouveau chasseur à la liste de chasseurs (ie, le hangar)
     */
    public void creerUnChasseur(String type, String etat)
    {
        // TODO
        Chasseur chasseur = new Chasseur(type, etat);
    }
    
    /**
     * Afficher les informations relatives à un chasseur
     */
    public void afficherUnChasseur(int input)
    {
        // TODO
        System.out.print("Identifiant : " + input);
        Chasseur.getHangar().get(input - 1).afficher();
    }
    
    /**
     * Afficher les informations relatives à tous les chasseurs contenus dans le hangar
     */
    public void afficherDesChasseurs()
    {
        // TODO
        for (Chasseur chasseur : Chasseur.getHangar()){
            System.out.print("Identifiant du chasseur : " + (Chasseur.getHangar().indexOf(chasseur) + 1));
            chasseur.afficher();
        }
    }
    
    /**
     * Afficher le nombre de chasseurs dont l'état est opérationnel
     */

    public void nbChasseursOperationnels()
    {
        // TODO
        int nbChasseursOperationnels = 0;
        for (int i = 0; i < Chasseur.getHangar().size(); i++) {
            if (Chasseur.getHangar().get(i).getEtat().equals("Opérationnel")) {
                nbChasseursOperationnels++;
            }
        }
        
        System.out.println(nbChasseursOperationnels);
    }
    
    /**
     * Associer un pilote à un chasseur
     */
    
    public void associer(int id1, int id2) {
        // TODO
        Chasseur.getHangar().get(id2-1).setPilote();
        Pilote.getEscadrille().get(id1-1).setChasseur();
    }
    
    /**
     * Modifier l'état d'un chasseur à partir de son identifiant
     */
    
    public void modifierEtatChasseur(int input, String input2){
        // TODO
        Chasseur.getHangar().get(input - 1).setEtat(input2);
    }
    
    // PARTIE 2 : COMMANDES EN LIEN AVEC LA CLASSE PILOTE
    
    /**
     * Créer & Ajouter un nouveau pilote à la liste de pilotes (ie, l'escadrille)
     */
    
    public void creerUnPilote(String nom, String prenom, int age, String race) {
        // TODO
        Pilote pilote = new Pilote(nom, prenom, age, race);
    }
    
    /**
     * Afficher les informations relatives à un pilote
     */
    
    public void afficherUnPilote(int input) {
        // TODO
        System.out.print("Identifiant : " + input);
        Pilote.getEscadrille().get(input - 1).afficher();
    }
    
    /**
     * Afficher les informations relatives à tous les pilotes contenus dans l'escadrille
     */
    public void afficherDesPilotes()
    {
        // TODO
        for (Pilote pilote : Pilote.getEscadrille()){
            System.out.print("Identifiant du pilote : " + (Pilote.getEscadrille().indexOf(pilote) + 1));
            pilote.afficher();
        }
    }
}