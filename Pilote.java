import java.util.ArrayList;

/**
 * Décrivez votre classe Chasseur ici.
 *
 * @author Yassine
 * @version v2 du 04 mai 2021
 */

public class Pilote
{
    private String nom;
    private String prenom;
    private int age;
    private String race;
    private Chasseur avion;
    private boolean b;
    private static ArrayList<Pilote> escadrille = new ArrayList<Pilote>();

    /**
    * Créé un pilote sans chasseur atribué
    */
    public Pilote(String nom, String prenom, int age, String race){
        this.nom = nom;
        this.prenom = prenom;
        
        if(age>=10 && age<=800) {
            this.age=age;
        } else if(age<10) {
            System.out.println("Vous etes trop jeune attendez encore qlq années");
        } else {
        System.out.println("Vous etes trop vieux laisser la place aux jeunes");
        }
        
        this.race = race;
        this.avion = null;
        this.b = true;
        escadrille.add(this);
    }
    
    /**
     * Getter : escadrille
     * Retourner la liste des pilotes créés
     */
    
    public static ArrayList<Pilote> getEscadrille() {
        return escadrille;
    }
    
    /**
     * Getter : chasseur
     * Retourner le chasseur associé au pilote
     */
    public Chasseur getChasseur() {
        return avion;
    }
    
    /**
     * Getter : nom
     * Retourner le nom du pilote
     */
    public String getNom() {
        return nom;
    }
    
    /**
     * Getter : prénom
     * Retourner le prénom du pilote
     */
    public String getPrenom() {
        return prenom;
    }
    
    /**
     * Setter : chasseur
     * Modifier le chasseur associé au pilote
     */
    public void setChasseur() {
        int id1 = 0;
        int id2 = 0;
        if (estDisponible() == true) {
            this.avion = Chasseur.getHangar().get(id2);
            if (avion.getPilote() != Pilote.getEscadrille().get(id1)){
                avion.setPilote();
            }
        }
    }
    
    /**
     * Verifier que le pilote n'a pas déjà un chasseur associé
     * Si le pilote est disponible, alors il pourra etre associe à un chasseur
     */
    public boolean estDisponible() {
        if (this.avion != null) {
            b = false;
        }
        return b;
    }
    
    /**
     * Afficher un pilote
     */
    
    public void afficher() {
        System.out.print(" Nom : " + getNom() + ", Prénom " + getPrenom());
        
        if (this.avion != null) {
            System.out.println(", pilote un chasseur de type " + getChasseur().getType() + " se trouvant dans un état " + getChasseur().getEtat());
        } else {
            System.out.println(", pilote le chasseur n° " + getChasseur());
        }
    }
}