import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
* @author Claire_Justin_Yassine
* @version v2 du 04 mai 2021
*/
public class Page3 extends JFrame
{
    private JButton bouton31;
    private JButton bouton32;
    private JButton bouton33;
    private JButton bouton34;
    private JButton bouton35;
    private JButton bouton36;
    
    public Page3()
    {
        JFrame f3 = new JFrame();
        this.setSize(600, 300);
        this.setLocationRelativeTo(null);
        this.setTitle("Action Chasseurs");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        
        JPanel p = new JPanel();
        p.setBackground(Color.BLACK);
        this.setContentPane(p);
        this.setVisible(true);
        
        JButton bouton31 = new JButton("Liste des Chasseurs");
        bouton31.setPreferredSize(new Dimension(180, 130));
        JButton bouton32 = new JButton("Choisir un chasseur");
        bouton32.setPreferredSize(new Dimension(180, 130));
        JButton bouton33 = new JButton("Ajouter un chasseur");
        bouton33.setPreferredSize(new Dimension(180, 130));
        JButton bouton34 = new JButton("Modifier l'état d'un chasseur");
        bouton34.setPreferredSize(new Dimension(180, 130));
        JButton bouton35 = new JButton("Quitter");
        bouton35.setPreferredSize(new Dimension(180, 130));
        JButton bouton36 = new JButton(" ");
        bouton36.setPreferredSize(new Dimension(180, 130));
        p.add(bouton31);
        p.add(bouton32);
        p.add(bouton33);
        p.add(bouton34);
        p.add(bouton35);
        p.add(bouton36);
        
        // Bouton Lister tous les chasseurs
        bouton31.addActionListener(
        new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if ((JButton)e.getSource() == bouton31)
                {
                    new Page31().setVisible(true);
                }
            }
        }
        );
        
        // Bouton Identifier un chasseur
        bouton32.addActionListener(
        new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if ((JButton)e.getSource() == bouton32)
                {
                    new Page32().setVisible(true);
                }
            }
        }
        );
        
        // Bouton Ajouter un chasseur
        bouton33.addActionListener(
        new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if ((JButton)e.getSource() == bouton33)
                {
                    new Page33().setVisible(true);
                }
            }
        }
        );
        
        // Bouton Modifier l'état d'un chasseur
        bouton34.addActionListener(
        new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if ((JButton)e.getSource() == bouton34)
                {
                    new Page34().setVisible(true);
                }
            }
        }
        );
        
        // Bouton quitter
        bouton35.addActionListener(
        new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if ((JButton)e.getSource() == bouton35)
                {
                    System.exit(0);
                }
            }
        }
        );

    }
}